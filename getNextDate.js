const MongoClient = require('mongodb').MongoClient;
const moment = require('moment');
const uri = 'mongodb+srv://test:test123456@cluster0.sefmu.mongodb.net/calendar?retryWrites=true&w=majority';
const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const getNextDate = (date, dayCount) => {
    return new Promise((resolve) => {
        if (!moment.utc(date).isValid()) {
            throw 'Дата имеет не верный формат';
        }
        if (!Number.isInteger(dayCount)) {
            throw 'Кол-во дней должно быть целым числом';
        }
        if (Number.isInteger(dayCount) && dayCount < 1) {
            throw 'Кол-во дней не может быть меньше 1';
        }
        client.connect((err) => {
            if (err) {
                throw err;
            }
            const collection = client.db('calendar').collection('weekends');
            let endDate = moment.utc(date).startOf('day');
            let addedDay = 0;
            const addDay = (endDate) => {
                collection.findOne({ dateTimeStamp: endDate.unix() }).then(async (doc) => {
                    endDate = endDate.add(1, 'day');
                    if (doc === null) {
                        addedDay++;
                    }
                    if (addedDay < dayCount) {
                        addDay(endDate);
                    } else {
                        collection.findOne({ dateTimeStamp: endDate.unix() }).then(async (doc) => {
                            if (doc === null) {
                                resolve(endDate);
                            } else {
                                addDay(endDate);
                            }
                        });
                    }
                });
            };
            addDay(endDate);
        });
    });
};

getNextDate('2021-01-01', 5).then((resultDate) => {
    console.log(resultDate.format('DD.MM.YYYY'));
    client.close();
});
