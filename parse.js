const osmosis = require('osmosis');
const moment = require('moment');

const yearsList = [2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021];

const MongoClient = require('mongodb').MongoClient;
const uri = 'mongodb+srv://test:test123456@cluster0.sefmu.mongodb.net/calendar?retryWrites=true&w=majority';
const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const processYearMonth = (data, year, month, collection) => {
        for (let day of data.weekends) {
            const date = moment().utc().year(year).month(month).date(parseInt(day)).startOf('day');
             collection.insertOne({ dateTimeStamp: date.unix(), isWeekend: true }).then(() => {
                 console.log(`Запись успешно добавлена: ${date.format()}`);
             });
        }
}

const parseCalendar = () => {
    client.connect((err) => {
        if (err) {
            throw err;
        }
        const collection = client.db('calendar').collection('weekends');
        for (const year of yearsList) {
            let monthIndex = 0;
            osmosis
                .get(`www.consultant.ru/law/ref/calendar/proizvodstvennye/${year}/`)
                .find('table.cal')
                .set({ weekends: ['td.weekend'] })
                .data((result) => {
                    processYearMonth(result, year, monthIndex, collection);
                    monthIndex++;
                });
        }
    });
    setTimeout(() => {
        client.close();
    }, 25000);
};

parseCalendar();
